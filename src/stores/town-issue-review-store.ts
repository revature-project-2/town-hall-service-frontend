import { createStore, Reducer } from "redux";
import {persistStore, persistReducer} from "redux-persist";
import storage from "redux-persist/lib/storage";
import { DateFormatter } from "../helper-classes/date-formatter";

// interface for intellisense
export interface TownIssueReviewState{
    currentIssuesDisplayed: [],
    isReviewIssueView: boolean,
    issueFilterType: string,
    filterKind: string,
    filterDate: string,
    meetings: [],
    issuesReport: {}
}

// Town Issue Review internal state
const initialState: TownIssueReviewState ={
    currentIssuesDisplayed: [],
    isReviewIssueView: false,
    issueFilterType: 'all',
    filterKind: 'all',
    filterDate: DateFormatter.todayFormattedDateString(),
    meetings: [],
    issuesReport: {}
}

// Redux-persist configuration
const persistConfig = {
    key: "root",
    storage
};


// Redux reducer
const reducer:Reducer = (currentTownIssueReviewState: TownIssueReviewState = initialState, action:any) => {
    switch (action.type) {
        case "updateCurrentIssuesDisplayed":
            const newCurrentIssuesDisplayed:TownIssueReviewState = {
                currentIssuesDisplayed: action.currentIssuesDisplayed,
                isReviewIssueView: currentTownIssueReviewState.isReviewIssueView,
                issueFilterType: currentTownIssueReviewState.issueFilterType,
                filterKind: currentTownIssueReviewState.filterKind,
                filterDate: currentTownIssueReviewState.filterDate,
                meetings: currentTownIssueReviewState.meetings,
                issuesReport: currentTownIssueReviewState.issuesReport
            };
            return newCurrentIssuesDisplayed;
        case "updateIsReviewIssueView":
            const newIsReviewIssueView:TownIssueReviewState = {
                currentIssuesDisplayed: currentTownIssueReviewState.currentIssuesDisplayed,
                isReviewIssueView: action.isReviewIssueView,
                issueFilterType: currentTownIssueReviewState.issueFilterType,
                filterKind: currentTownIssueReviewState.filterKind,
                filterDate: currentTownIssueReviewState.filterDate,
                meetings: currentTownIssueReviewState.meetings,
                issuesReport: currentTownIssueReviewState.issuesReport
            };
            return newIsReviewIssueView;
        case "updateIssueFilterType":
            const newIssueFilter:TownIssueReviewState = {
                currentIssuesDisplayed: currentTownIssueReviewState.currentIssuesDisplayed,
                isReviewIssueView: currentTownIssueReviewState.isReviewIssueView,
                issueFilterType: action.issueFilterType,
                filterKind: currentTownIssueReviewState.filterKind,
                filterDate: currentTownIssueReviewState.filterDate,
                meetings: currentTownIssueReviewState.meetings,
                issuesReport: currentTownIssueReviewState.issuesReport
            };
            return newIssueFilter;
        case "updateFilterKind":
            const newFilterKind:TownIssueReviewState = {
                currentIssuesDisplayed: currentTownIssueReviewState.currentIssuesDisplayed,
                isReviewIssueView: currentTownIssueReviewState.isReviewIssueView,
                issueFilterType: currentTownIssueReviewState.issueFilterType,
                filterKind: action.filterKind,
                filterDate: currentTownIssueReviewState.filterDate,
                meetings: currentTownIssueReviewState.meetings,
                issuesReport: currentTownIssueReviewState.issuesReport
            };
            return newFilterKind;
        case "updateFilterDate":
            const newFilterDate:TownIssueReviewState = {
                currentIssuesDisplayed: currentTownIssueReviewState.currentIssuesDisplayed,
                isReviewIssueView: currentTownIssueReviewState.isReviewIssueView,
                issueFilterType: currentTownIssueReviewState.issueFilterType,
                filterKind: currentTownIssueReviewState.filterKind,
                filterDate: action.filterDate,
                meetings: currentTownIssueReviewState.meetings,
                issuesReport: currentTownIssueReviewState.issuesReport
            };
            return newFilterDate;
        case "updateMeetings":
            const newMeetings:TownIssueReviewState = {
                currentIssuesDisplayed: currentTownIssueReviewState.currentIssuesDisplayed,
                isReviewIssueView: currentTownIssueReviewState.isReviewIssueView,
                issueFilterType: currentTownIssueReviewState.issueFilterType,
                filterKind: currentTownIssueReviewState.filterKind,
                filterDate: currentTownIssueReviewState.filterDate,
                meetings: action.meetings,
                issuesReport: currentTownIssueReviewState.issuesReport
            };
            return newMeetings;
        case "updateIssuesReport":
            const newIssuesReport:TownIssueReviewState = {
                currentIssuesDisplayed: currentTownIssueReviewState.currentIssuesDisplayed,
                isReviewIssueView: currentTownIssueReviewState.isReviewIssueView,
                issueFilterType: currentTownIssueReviewState.issueFilterType,
                filterKind: currentTownIssueReviewState.filterKind,
                filterDate: currentTownIssueReviewState.filterDate,
                meetings: currentTownIssueReviewState.meetings,
                issuesReport: action.issuesReport
            };
            return newIssuesReport;
        default:
            return currentTownIssueReviewState;
    }
};

const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(persistedReducer, initialState);

const persistor = persistStore(store);

// export default store;

export {store, persistor}