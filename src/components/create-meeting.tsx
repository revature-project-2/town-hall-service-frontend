import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Checkbox from "@mui/material/Checkbox";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import * as React from 'react';
import { useDispatch } from "react-redux";
import { MeetingAPI } from "../apis/meetings-api";
import { TimeFormatter } from "../helper-classes/time-converter";
import { types } from "../types-of-issues";
import ErrorAlert from "./error-alert";
import SuccessAlert from "./success-alert";

export default function CreateMeeting(){
    // Get form input values
    const timeInput = React.useRef<any>(null);
    const dateInput = React.useRef<any>(null);
    const locationInput = React.useRef<any>(null);
    const passInp = React.useRef<any>(null);
    const dispatch = useDispatch();
    let typesInput: string[] = [];

    // Alert handle open/close
    const [openSuccess, setOpen] = React.useState(false);
    const [openError, setOpenError] = React.useState(false);

    const closeSuccessAlert = () =>{
        setOpen(false);
    };
    const openSuccessAlert = () =>{
        setOpen(true);
    };

    const closeErrorAlert = () =>{
        setOpenError(false);
    };
    const openErrorAlert = () =>{
        setOpenError(true);
    };

    const clearFrom = ()=>{
        timeInput.current.value = null;
        dateInput.current.value = null;
        locationInput.current.value = null;
        passInp.current.value = null;
        typesInput = [];
    };

    const createMeeting = ()=>{
        const meeting = {
            m_location: locationInput.current.value,
            m_date: new Date(dateInput.current.value).toLocaleDateString(),
            m_time: TimeFormatter.formatTime(timeInput.current.value),
            topics: typesInput
        };
        if (passInp.current.value === "p"){
            MeetingAPI.createMeetingAuth(meeting)
            .then((response) => {
                MeetingAPI.getMeetings()
                    .then((response) => {
                        const data = response.data;
                        dispatch({ type: 'updateMeetings', meetings: data });
                        openSuccessAlert();
                    })
                    .catch((error) => {
                        console.log(error);
                        openErrorAlert();
                    })
            })
            .catch((error) => {
                console.log(error);
            })
        } else {
            MeetingAPI.createMeeting(meeting)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
                openErrorAlert();
            })
        }
        clearFrom();
    };

    const getTypes = (e:any)=>{
        
        switch (e.target.id) {
            case 'Infrastructure':
                if (e.target.checked) {
                    typesInput.push("Infrastructure")
                } else {
                    typesInput.splice(typesInput.indexOf("Infrastructure"), 1);
                }
                break;
            case 'Safety':
                if (e.target.checked) {
                    typesInput.push("Safety");
                } else {
                    typesInput.splice(typesInput.indexOf("Safety"), 1);
                }
                break;
            case 'Public Health':
                if (e.target.checked) {
                    typesInput.push("Public Health");
                } else {
                    typesInput.splice(typesInput.indexOf("Public Health"), 1);
                }
                break;
            case 'Pollution':
                if (e.target.checked) {
                    typesInput.push("Pollution");
                } else {
                    typesInput.splice(typesInput.indexOf("Pollution"), 1);
                }
                break;
            case 'Noise/Disturbing the peace':
                if (e.target.checked) {
                    typesInput.push("Noise");
                } else {
                    typesInput.splice(typesInput.indexOf("Noise"), 1);
                }
                break;
            case 'Other':
                if (e.target.checked) {
                    typesInput.push("Other");
                } else {
                    typesInput.splice(typesInput.indexOf("Other"), 1);
                }
                break;
            default:
                break;
        };        
    };

    function CreateMeetingForm(){
        return(
        <>
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />
        <Grid
         container
         direction="row"
         justifyContent="center"
         alignItems="center"
        >
            <Grid item xs={12} >
                <Card >
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                        Create a Meeting
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                        Please fill out the form to create a meeting.
                        </Typography>

                    <div>
                    <TextField
                        required
                        margin="dense"
                        id="recipient"
                        label="Location"
                        type="input"
                        variant="standard"
                       inputRef={locationInput}
                        fullWidth
                    />
                    </div>

                    <div>
                    <TextField
                        required
                        margin="dense"
                        id="recipient"
                        // label="Date of meeting"
                        type="date"
                        inputRef={dateInput}
                        variant="standard"
                        fullWidth
                        />
                    </div>

                    <div>
                    <TextField
                        required
                        margin="dense"
                        id="recipient"
                        // label="Time of meeting"
                        type="time"
                        inputRef={timeInput}
                        variant="standard"
                        fullWidth
                        />
                    </div>

                    <div>
                    <FormControl component="fieldset" variant="standard">
                    <FormGroup>
                    <Grid container>
                      {types.slice(1).map((t, i)=>  
                      <Grid item key={i}>
                        <FormControlLabel control={<Checkbox id={t} onChange={(e)=>{getTypes(e)}} />} label={t} value={t}/>
                      </Grid>)}
                    </Grid>
                    </FormGroup>
                    </FormControl>  
                    </div>

                    </CardContent>
                    <CardActions>
                    <Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-end"
                    spacing={4}
                    >
                        <Grid item xs={3}>
                            <TextField autoFocus margin="none" id="Password" label="Password" inputRef={passInp} fullWidth variant="standard" />
                        </Grid>
                        <Grid item xs={3}>
                        <Button size="small" onClick={createMeeting}>Create Meeting</Button>
                        </Grid>
                    </Grid>
                    </CardActions>
                </Card>
            </Grid>
        </Grid>
        </Box>
        </>
        );
    }
    return(
    <>
    <CreateMeetingForm/>
    <SuccessAlert open={openSuccess} closeAlert={closeSuccessAlert} message={'Meeting was successfully created.'}/>
    <ErrorAlert open={openError} closeAlert={closeErrorAlert} message={'There was an error creating the meeting. Please make sure you have the right credentials and fill out the entire form.'}/>
    </>
    );
}