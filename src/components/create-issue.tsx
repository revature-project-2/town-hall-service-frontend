import Grid from "@mui/material/Grid";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import TextField from "@mui/material/TextField";
import SuccessAlert from "./success-alert";
import axios from "axios";
import ErrorAlert from "./error-alert";
import { types } from "../types-of-issues";
import { DateFormatter } from "../helper-classes/date-formatter";

export default function CreateIssue(){
    // Alert handle open/close
    const [openSuccess, setOpenSuccess] = React.useState(false);
    const [openError, setOpenError] = React.useState(false);

    // Get form input values
    const descriptionInput = React.useRef<any>(null);
    const locationInput = React.useRef<any>(null);
    const dateOfIssueInput = React.useRef<any>(null);
    let typeInput = '';

    const closeSuccessAlert = () =>{
        setOpenSuccess(false);
    };
    const openSuccessAlert = () =>{
        setOpenSuccess(true);
    };

    const closeErrorAlert = () =>{
        setOpenError(false);
    };
    const openErrorAlert = () =>{
        setOpenError(true);
    };

    const clearFrom = ()=>{
        descriptionInput.current.value = null;
        locationInput.current.value = null;
        dateOfIssueInput.current.value = null;
    };

    const createIssue = ()=>{
        //create issue body
        const issue = {
            description: descriptionInput.current.value,
            location: locationInput.current.value,
            // dateOfIssue: new Date(dateOfIssueInput.current.value),
            dateOfIssue: DateFormatter.formatDateString(dateOfIssueInput.current.value),
            // dateOfPosted: new Date(today.substring(0, i)),
            dateOfPosted: DateFormatter.todayFormattedDateString(),
            type: typeInput
        }
        console.log(dateOfIssueInput.current.value);
        console.log(issue);
        
        // Send publish issue to the issue-topic
        axios.post('https://issue-ingestion-ovjxki6iua-uc.a.run.app/submitissue', issue)
        .then((response)=>{
            console.log(response);
            openSuccessAlert();
        }).catch((error)=>{
            console.log(error);
            clearFrom();
            openErrorAlert();
        });
     
    };

    const getIssueType = (type:any)=>{
        console.log(type);
        typeInput = type;
        
    };

    function CreateIssueForm(){
        return(
        <>
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />
        <Grid
         container
         direction="row"
         justifyContent="center"
         alignItems="center"
        >
            <Grid item xs={12} >
                <Card >
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                        Create an Issue
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                        Please fill out the form to create an issue.
                        </Typography>

                    <div>
                    <TextField
                        required
                        margin="dense"
                        id="recipient"
                        label="Description"
                        type="input"
                        variant="standard"
                        inputRef={descriptionInput}
                        fullWidth
                    />
                    </div>

                    <div>
                    <TextField
                        required
                        margin="dense"
                        id="recipient"
                        label="Location"
                        type="input"
                        variant="standard"
                       inputRef={locationInput}
                        fullWidth
                    />
                    </div>

                    <div>
                    <TextField
                        required
                        margin="dense"
                        id="recipient"
                        // label="Date of issue"
                        type="date"
                        inputRef={dateOfIssueInput}
                        variant="standard"
                        fullWidth
                        helperText="Please select the date of the issue"
                        />
                    </div>

                    <div>
                    <TextField
                        variant="standard"
                        select
                        fullWidth
                        onChange={(e) =>{ getIssueType(e.target.value)}}
                        SelectProps={{
                        native: true,
                        }}
                        helperText="Please select the type of issue"
                    >
                        {types.map((option) => (
                        <option key={option} value={option}>
                            {option}
                        </option>
                        ))}
                    </TextField>
                    </div>
                    </CardContent>
                    <CardActions>
                        <Button size="small" onClick={createIssue}>send issue</Button>
                    </CardActions>
                </Card>
            </Grid>
        </Grid>
        </Box>
        </>
        );
    };

    return(
    <>
    <CreateIssueForm/>
    <SuccessAlert open={openSuccess} closeAlert={closeSuccessAlert} message={'Issue was successfully created'}/>
    <ErrorAlert open={openError} closeAlert={closeErrorAlert} message={'There was an error submitting your issue. Please make sure to fill out the entire form.'}/>
    </>
    );
}

