
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import * as React from 'react';
import axios from 'axios';
import Grid from "@mui/material/Grid";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import SuccessAlert from "./success-alert";
import ErrorAlert from "./error-alert";
import { types } from "../types-of-issues";
import { MeetingAPI } from '../apis/meetings-api';
import { useDispatch, useSelector } from 'react-redux';
import { Checkbox, FormControlLabel } from '@mui/material';

export default function FormDialog(props: any) {
    const meeting = props.meeting;
    const dateInp = React.useRef<any>(null);
    const locationInp = React.useRef<any>(null);
    const timeInp = React.useRef<any>(null);
    const passInp = React.useRef<any>(null);
    const [topicsInp, setTopics] = React.useState([] as any[]);
    const dispatch = useDispatch();
    // const [openSuccess, setOpenSuccess] = React.useState(false);
    // const [openError, setOpenError] = React.useState(false);
    const [open, setOpen] = React.useState(false);

    // Alert handle open/close
    const [openSuccess, setOpenSuccess] = React.useState(false);
    const [openError, setOpenError] = React.useState(false);

    const closeSuccessAlert = () =>{
        setOpenSuccess(false);
    };
    const openSuccessAlert = () =>{
        setOpenSuccess(true);
    };

    const closeErrorAlert = () =>{
        setOpenError(false);
    };
    const openErrorAlert = () =>{
        setOpenError(true);
    };


    const handleClickOpen = () => {
        setTopics(meeting.topics);
        setOpen(true);
    };
    const handleClose = () => {
        setTopics(meeting.topics);
        setOpen(false);
    };
    const clearForm = () => {
        dateInp.current.value = null;
        locationInp.current.value = null;
        timeInp.current.value = null;
        setTopics(meeting.topics);
    };
    const editMeeting = () => {

        const newMeeting = {
            id: meeting.id,
            m_location: locationInp.current.value ? locationInp.current.value : meeting.m_location,
            m_date: dateInp.current.value ? locationInp.current.value : meeting.m_date,
            m_time: timeInp.current.value ? timeInp.current.value : meeting.m_time,
            topics: topicsInp
        }
        console.log(newMeeting);
        
        if (passInp.current.value === "p"){
            MeetingAPI.updateMeetingAuth(meeting.id, newMeeting)
            .then((response) => {
                MeetingAPI.getMeetings()
                    .then((response) => {
                        const data = response.data;
                        dispatch({ type: 'updateMeetings', meetings: data });
                        openSuccessAlert();
                        setOpen(false);
                    })
                    .catch((error) => {
                        console.log(error);
                        openErrorAlert();
                    })
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
            })
        } else {
            MeetingAPI.updateMeeting(meeting.id, newMeeting)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
                openErrorAlert();
            })
        }
        // setOpen(false);
    };
    const handleCheckbox = (e: any) => {
        let tmp: string[] = topicsInp;
        switch (e.target.id) {
            case "Infrastructure":
                if (e.target.checked) {
                    tmp.push("Infrastructure")
                    setTopics([...tmp]);
                } else {
                    tmp.splice(tmp.indexOf("Infrastructure"), 1);
                    setTopics([...tmp]);
                }
                break;
            case "Safety":
                if (e.target.checked) {
                    tmp.push("Safety");
                    setTopics([...tmp])
                } else {
                    tmp.splice(tmp.indexOf("Safety"), 1);
                    setTopics([...tmp]);
                }
                break;
            case "Public Health":
                if (e.target.checked) {
                    tmp.push("Public Health");
                    setTopics([...tmp]);
                } else {
                    tmp.splice(tmp.indexOf("Public Health"), 1);
                    setTopics([...tmp]);
                }
                break;
            case "Pollution":
                if (e.target.checked) {
                    tmp.push("Pollution");
                    setTopics([...tmp]);
                } else {
                    tmp.splice(tmp.indexOf("Pollution"), 1);
                    setTopics([...tmp]);
                }
                break;
            case "Noise":
                if (e.target.checked) {
                    tmp.push("Noise");
                    setTopics([...tmp]);
                } else {
                    tmp.splice(tmp.indexOf("Noise"), 1);
                    setTopics([...tmp]);
                }
                break;
            case "Other":
                if (e.target.checked) {
                    tmp.push("Other");
                    setTopics([...tmp]);
                } else {
                    tmp.splice(tmp.indexOf("Other"), 1);
                    setTopics([...tmp]);
                }
                break;
            default:
                break;
        }
    }
    const deleteMeeting = () =>{
        if (passInp.current.value === "p"){
            MeetingAPI.deleteMeetingAuth(meeting.id)
            .then((response) => {
                MeetingAPI.getMeetings()
                    .then((response) => {
                        const data = response.data;
                        dispatch({ type: 'updateMeetings', meetings: data });
                        openSuccessAlert();
                    })
                    .catch((error) => {
                        console.log(error);
                        openErrorAlert();
                    })
                console.log(response);
            })
            .catch((error) => {
                console.log(error);
                openErrorAlert();
            })
        } else {
            MeetingAPI.deleteMeeting(meeting.id)
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
                openErrorAlert();
            })
        }
        passInp.current.value = null;
    }
    return (
        <div>
            <Button onClick={handleClickOpen}>
                Edit Meeting
            </Button>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Edit Meeting</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Only fill in fields that need changed.
                    </DialogContentText>
                    <TextField autoFocus margin="dense" id="location" label="Location" defaultValue={meeting.m_location} inputRef={locationInp} fullWidth variant="standard" />
                    <TextField margin="dense" id="recipient" type="date" inputRef={dateInp} InputLabelProps={{ shrink: true, }} label={'Current Date:' + ' ' + meeting.m_date} variant="standard" fullWidth />
                    <TextField autoFocus margin="dense" id="Time" label="Time" inputRef={timeInp} defaultValue={meeting.m_time} fullWidth variant="standard" />
                    <Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    >
                    <Grid item  xs={6}>
                        <FormControlLabel control={<Checkbox checked={topicsInp.includes("Infrastructure")} onChange={(e) => { handleCheckbox(e) }} id='Infrastructure' />} label="Infrastructure" />
                    </Grid>
                    <Grid item  xs={6}>
                        <FormControlLabel control={<Checkbox checked={topicsInp.includes("Safety")} onChange={(e) => { handleCheckbox(e) }} id='Safety' />} label="Safety" />
                    </Grid>
                    <Grid item  xs={6}>
                        <FormControlLabel control={<Checkbox checked={topicsInp.includes("Public Health")} onChange={(e) => { handleCheckbox(e) }} id='Public Health' />} label="Public Health" />
                    </Grid>
                    <Grid item  xs={6}>
                        <FormControlLabel control={<Checkbox checked={topicsInp.includes("Pollution")} onChange={(e) => { handleCheckbox(e) }} id='Pollution' />} label="Pollution" />
                    </Grid>
                    <Grid item  xs={6}>
                        <FormControlLabel control={<Checkbox checked={topicsInp.includes("Noise")} onChange={(e) => { handleCheckbox(e) }} id='Noise' />} label="Noise" />
                    </Grid>
                    <Grid item  xs={6}>
                        <FormControlLabel control={<Checkbox checked={topicsInp.includes("Other")} onChange={(e) => { handleCheckbox(e) }} id='Other' />} label="Other" />
                    </Grid>

                    </Grid>
                </DialogContent>
                <DialogActions>
                <Grid
                container
                direction="row"
                justifyContent="flex-end"
                alignItems="flex-end"
                >
                    <Grid item  xs={4}>
                        <TextField autoFocus margin="dense" id="Password" label="Password" inputRef={passInp} fullWidth variant="standard" />
                    </Grid>

                    <Grid item  xs={3}>
                        <Button fullWidth onClick={editMeeting}>Edit Meeting</Button>
                    </Grid>
                    <Grid item  xs={3}>
                        <Button fullWidth onClick={deleteMeeting}>Delete Meeting</Button>
                    </Grid>
                    <Grid item  xs={2}>
                        <Button fullWidth onClick={handleClose}>Cancel</Button>
                    </Grid>
                </Grid>
                </DialogActions>
            </Dialog>
              <SuccessAlert open={openSuccess} closeAlert={closeSuccessAlert} message={'Success!'}/>
            <ErrorAlert open={openError} closeAlert={closeErrorAlert} message={'There was an error while processing your request. Make sure you have the proper credentials'}/>
        </div>
    );
}

