import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Checkbox from "@mui/material/Checkbox";
import Grid from "@mui/material/Grid";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import { IssueAPI } from "../apis/issues-api";
import { useDispatch, useSelector } from "react-redux";
import { DateFormatter } from "../helper-classes/date-formatter";

export default function DisplayIssues(){
    const dispatch = useDispatch();
    const currentTownIssueReviewState = useSelector((state:any)=>state); 

    const handleCheckbox = (e:any, issue:any)=>{
        // edit the checked value
        e.target.id === 'reviewed'? issue.issue.isReviewed = e.target.checked : issue.issue.isHighlighted = e.target.checked;
        console.log(issue.issue);

        // Update the value in datatstore
        // get updated issues depending on filter case
        // send to redux store
        IssueAPI.updateIssue(Number(issue.id), issue.issue)
        .then((response)=>{
            console.log(response);
            switch (currentTownIssueReviewState.filterKind) {
                case 'all':
                    IssueAPI.getIssues()
                    .then((response)=>{
                        const data = response.data;
                        dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
                    })
                    .catch((error)=>{console.log(error);
                    });  
                break;
                case 'type':
                    IssueAPI.getIssuesByType(currentTownIssueReviewState.issueFilterType)
                    .then((response)=>{
                      console.log(response.data);
                      const data = response.data;
                      dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
                    })
                    .catch((error)=>{console.log(error);
                    });
                    break;
                case 'dateOfIssue':
                    console.log(currentTownIssueReviewState.filterDate);
                    
                    IssueAPI.getIssuesByDateOfIssue(currentTownIssueReviewState.filterDate)
                    .then((response)=>{
                        console.log(response.data);
                        const data = response.data;
                        dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
                    })
                    .catch((error)=>{console.log(error);
                    });
                    break;
                case 'dateOfPosted':
                    console.log(currentTownIssueReviewState.filterDate);
                    IssueAPI.getIssuesByDateOfPosted(currentTownIssueReviewState.filterDate)
                        .then((response) => {
                            console.log(response.data);
                            const data = response.data;
                            dispatch({ type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                    break;
                case 'isReviewed':
                    IssueAPI.getIssuesByReviewed('true')
                    .then((response)=>{
                        console.log(response.data);
                        const data = response.data;
                        dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
                    })
                    .catch((error)=>{console.log(error);
                    });
                    break;
                case 'isHighlighted':
                    IssueAPI.getIssuesByHighlighted('true')
                    .then((response)=>{
                        console.log(response.data);
                        const data = response.data;
                        dispatch({type: 'updateCurrentIssuesDisplayed', currentIssuesDisplayed: data });
                    })
                    .catch((error)=>{console.log(error);
                    });
                    break;
                default:
                    break;
            }
            
        })
        .catch((error)=>{
            console.log(error);
        });

    };

    const issueCards = currentTownIssueReviewState.currentIssuesDisplayed.map((issue:any, index:any)=>{
        
        return (<Grid item xs={6} key={index}>
               <Card className={issue.issue.isHighlighted ? 'highlighted' : ''}>

                <CardContent >
                <Typography gutterBottom variant="h5" component="div">
                {`Posted: ${DateFormatter.fixUTCDateVariance(issue.issue.dateOfPosted)}`}
                </Typography>
                <Typography gutterBottom variant="h5" component="div">
                    {`Type: ${issue.issue.type} `}  
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {`Description: ${issue.issue.description}`}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {`Location: ${issue.issue.location}`}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {`Date of issue: ${DateFormatter.fixUTCDateVariance(issue.issue.dateOfIssue)}`}
                </Typography>
                </CardContent>
                <CardActions>
                <FormControl component="fieldset" variant="standard">
                    <FormGroup>
                    <Grid container>
                    <Grid item>
                        <FormControlLabel control={<Checkbox checked={issue.issue.isReviewed} onChange={(e)=>{handleCheckbox(e, issue)}} id='reviewed' />} label="Mark as reviewed" />
                    </Grid>   
                    <Grid item>
                        <FormControlLabel control={<Checkbox checked={issue.issue.isHighlighted} onChange={(e)=>{handleCheckbox(e, issue)}} id='highlighted' />} label="highlight" />
                    </Grid> 
                    </Grid>
                    </FormGroup>
                </FormControl>
                </CardActions>
                </Card>
        </Grid>);
    });

    return (
    <>
    <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
    <Toolbar /> 
        <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="flex-start"
        spacing={4}
        >
           {issueCards}
        </Grid>      
    </Box>
    </>
    );
}