import Alert from "@mui/material/Alert";
import Snackbar from "@mui/material/Snackbar";

export default function SuccessAlert(props:any){
    return (
    <>
    <Snackbar open={props.open} autoHideDuration={6000} onClose={props.closeAlert}>
        <Alert onClose={props.closeAlert} severity="success" sx={{ width: '100%' }}>
        {props.message}
        </Alert>
      </Snackbar>
    </>
    );
};