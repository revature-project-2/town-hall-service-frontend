import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

export default function HomePage(){
    return(
        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />
        <Typography variant='h3' >
        Welcome to the Town Meeting Issue Board. 
        </Typography>

        <Typography variant='body1'>
        The purpose of this application is to address any issues in our community and have them reviewed by the city council for prompt resolution. 
        </Typography>
        <Typography variant='body1'>
        Please create an issue by selecting "Create Issue" on the side menu.  We work hard to resolve the issues quickly, but please be patient as the process takes some time. 
        </Typography>
        <Typography variant='body1'>
        Check back often to see if a meeting addressing an issue of interest has been posted.
        </Typography>
      </Box>
    );
}