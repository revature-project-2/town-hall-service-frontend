import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React from "react";
// import { meetings } from "../dummy-meetings";
import { useRef } from "react";
import { useSelector } from "react-redux";
import ErrorAlert from "./error-alert";
import FormDialog from "./meeting-dialog";
import SuccessAlert from "./success-alert";

export default function DisplayMeetings() {

    const currentTownIssueReviewState = useSelector((state: any) => state);
    const locationInp = useRef(null);
    const dateInp = useRef(null);
    const timeInp = useRef(null);


    const meetingCards = currentTownIssueReviewState.meetings.map((meeting: any, index: any) => {
        return (<Grid item xs={6} key={index}>
            <Card>
                <CardContent >
                    <Typography gutterBottom variant="h5" component="div">
                        {`Meeting ID: ${meeting.id}`}
                    </Typography>
                    <Typography gutterBottom variant="h5" component="div">
                        {`Location: ${meeting.m_location} `}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {`Date and Time: ${meeting.m_date} ${meeting.m_time}`}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {`Topics: ${meeting.topics}`}
                    </Typography>
                </CardContent>
                <CardActions>
                    <FormDialog meeting={meeting}/>
                </CardActions>
            </Card>
        </Grid>);
    });


    return (
        <>
            <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                <Toolbar />
                <Grid
                    container
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="flex-start"
                    spacing={4}
                >
                    {meetingCards}
                </Grid>
            </Box>
        </>
    );
}