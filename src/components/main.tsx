import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import { Route } from "react-router-dom";
import AppBarNav from "./app-bar-nav";
import CreateIssue from './create-issue'
import CreateMeeting from "./create-meeting";
import DisplayIssues from "./display-issues";
import DisplayMainBody from "./display-main-body";
import DisplayMeetings from "./display-meetings";
import HomePage from "./home-page";
import SideMenu from "./side-menu";
import IssuesReport from "./view-issue-report";

export default function Main() {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBarNav />
                <SideMenu />

                <Route path="/home">
                    <HomePage />
                </Route>

                <Route path="/create-issue">
                    <CreateIssue />
                </Route>

                <Route path="/create-meeting">
                    <CreateMeeting />
                </Route>

                <Route path="/review-issues">
                    <DisplayIssues />
                </Route>

                <Route path="/view-meetings">
                    <DisplayMeetings />
                </Route>

                <Route path="/issues-report">
                    <IssuesReport/>
                </Route>

            </Box>
        </>
    );
}
