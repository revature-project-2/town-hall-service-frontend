export class DateFormatter{
    static todayFormattedDateString(){
         //Clean up date string
         let today = (new Date()).toISOString();
         let i = today.indexOf('T');
         return (new Date(today.substring(0, i))).toISOString(); 
    };

    static formatDateString(date:string){
        return (date !== '' ? (new Date(date)).toISOString() : (new Date()).toISOString());
    };

    static fixUTCDateVariance(date:string){
        const tempDate = ( date !== '' ? (new Date(date)).toUTCString() : (new Date()).toUTCString());
        const i = tempDate.indexOf(':');
        return tempDate.substring(0, i - 3);
    };
}