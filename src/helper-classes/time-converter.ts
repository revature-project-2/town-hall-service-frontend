export class TimeFormatter{
    static formatTime(time:string){
        let timeString:string = '';
        const hour = Number(time.substring(0, 2));
        const min = time.substring(3, );

        if(Number(time.substring(0, 2)) <= 12){
            timeString = `${hour}: ${min} AM`;
        }else {

            timeString = `${hour-12}:${min} PM`;
        }

        return timeString;
    }
}